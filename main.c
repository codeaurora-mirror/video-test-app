/*
 * V4L2 Codec decoding example application
 * Kamil Debski <k.debski@samsung.com>
 *
 * Main file of the application
 *
 * Copyright 2012 Samsung Electronics Co., Ltd.
 * Copyright (c) 2015 Linaro Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include <stdio.h>
#include <string.h>
#include <linux/videodev2.h>
#include <linux/media.h>
#include <sys/ioctl.h>
#include <sys/time.h>
#include <poll.h>
#include <pthread.h>
#include <semaphore.h>
#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdint.h>
#include <stdlib.h>

#include "args.h"
#include "common.h"
#include "fileops.h"
#include "video.h"
#include "gentest.h"

/* This is the size of the buffer for the compressed stream.
 * It limits the maximum compressed frame size. */
#define STREAM_BUFFER_SIZE	(1024 * 1024)

/* The number of compress4ed stream buffers */
#define STREAM_BUFFER_CNT	2

/* The number of extra buffers for the decoded output.
 * This is the number of buffers that the application can keep
 * used and still enable video device to decode with the hardware. */
#define RESULT_EXTRA_BUFFER_CNT 2

static const int event_types[] = {
	V4L2_EVENT_EOS,
	V4L2_EVENT_SOURCE_CHANGE,
};

static int subscribe_for_events(int fd)
{
	int size_event = sizeof(event_types) / sizeof(event_types[0]);
	struct v4l2_event_subscription sub;
	int i, ret;

	for (i = 0; i < size_event; i++) {
		memset(&sub, 0, sizeof(sub));
		sub.type = event_types[i];
		ret = ioctl(fd, VIDIOC_SUBSCRIBE_EVENT, &sub);
		if (ret < 0)
			err("cannot subscribe for event type %d (%s)",
				sub.type, strerror(errno));
	}

	return 0;
}

static int handle_v4l_events(struct video *vid)
{
	struct v4l2_event event;
	int ret;

	memset(&event, 0, sizeof(event));
	ret = ioctl(vid->fd, VIDIOC_DQEVENT, &event);
	if (ret < 0) {
		err("vidioc_dqevent failed (%s) %d", strerror(errno), -errno);
		return -errno;
	}

	switch (event.type) {
	case V4L2_EVENT_EOS:
		info("EOS reached");
		break;
	case V4L2_EVENT_SOURCE_CHANGE:
		info("Source changed");
		break;
	default:
		dbg("unknown event type occurred %x", event.type);
		break;
	}

	return 0;
}

static void cleanup(struct instance *i)
{
	if (i->video.fd)
		video_close(i);
	if (i->in.fd)
		input_close(i);
	if (i->out.fd)
		close(i->out.fd);
}

//specific to VP8
static void mem_put_le16(void *vmem, int val)
{
	uint8_t *mem = (uint8_t *)vmem;

	mem[0] = (uint8_t)((val >> 0) & 0xff);
	mem[1] = (uint8_t)((val >> 8) & 0xff);
}

static void mem_put_le32(void *vmem, int val)
{
	uint8_t *mem = (uint8_t *)vmem;

	mem[0] = (uint8_t)((val >>  0) & 0xff);
	mem[1] = (uint8_t)((val >>  8) & 0xff);
	mem[2] = (uint8_t)((val >> 16) & 0xff);
	mem[3] = (uint8_t)((val >> 24) & 0xff);
}

#define VP8_FOURCC	0x30385056
#define VP9_FOURCC	0x30395056

static void ivf_write_file_header(int outfile, off_t offset, unsigned int width,
				  unsigned int height, unsigned int fourcc,
				  int frame_cnt, int timebase_den,
				  int timebase_num)
{
	char header[32];

	header[0] = 'D';
	header[1] = 'K';
	header[2] = 'I';
	header[3] = 'F';
	mem_put_le16(header + 4, 0);			// version
	mem_put_le16(header + 6, 32);			// header size
	mem_put_le32(header + 8, fourcc);		// fourcc
	mem_put_le16(header + 12, width);		// width
	mem_put_le16(header + 14, height);		// height
	mem_put_le32(header + 16, timebase_den);	// rate
	mem_put_le32(header + 20, timebase_num);	// scale
	mem_put_le32(header + 24, frame_cnt);		// length
	mem_put_le32(header + 28, 0);			// unused

	pwrite(outfile, header, 32, offset);
}

static void ivf_write_frame_header(int outfile, off_t offset, size_t frame_size)
{
	char header[12];
	static int64_t pts = 0;

	mem_put_le32(header, (int)frame_size);
	mem_put_le32(header + 4, (int)(pts & 0xFFFFFFFF));
	mem_put_le32(header + 8, (int)(pts >> 32));

	pwrite(outfile, header, 12, offset);

	pts++;
}

static int save_encoded(struct instance *i, const void *buf, unsigned int size,
			bool is_ivf)
{
	mode_t mode = S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH;
	char filename[64];
	int ret;
	struct output *out = &i->out;
	ssize_t written;
	const char *ext = i->codec_name;

	if (!i->save_encoded)
		return 0;

	if (is_ivf)
		ret = sprintf(filename, "%s/encoded.%s.ivf", i->save_path, ext);
	else
		ret = sprintf(filename, "%s/encoded.%s", i->save_path, ext);
	if (ret < 0) {
		err("sprintf fail (%s)", strerror(errno));
		return -1;
	}

	if (out->fd)
		goto write;

	out->fd = open(filename, O_WRONLY | O_CREAT | O_TRUNC | O_SYNC, mode);
	if (out->fd < 0) {
		err("cannot open file (%s)", strerror(errno));
		return -1;
	}

	dbg("created file %s", filename);

	if (is_ivf) {
		ivf_write_file_header(out->fd, out->offs, i->width, i->height,
				      VP8_FOURCC, 0, 30, 1);
		out->offs += 32;
	}
write:
	if (is_ivf) {
		dbg("frame_header: %d, %d", out->offs, size);
		ivf_write_frame_header(out->fd, out->offs, size);
		out->offs += 12;
	}

	written = pwrite(out->fd, buf, size, out->offs);
	if (written < 0) {
		err("cannot write to file (%s)", strerror(errno));
		return -1;
	}

	out->offs += written;

	dbg("written %zd bytes at offset %zu", written, out->offs);

	return 0;
}

static int input_read(struct instance *inst, unsigned int index,
		      unsigned int *used, unsigned int *fs)
{
	struct video *vid = &inst->video;
	unsigned char *to = vid->out_buf_addr[index];

	*used = vid->out_buf_size;
	*fs = vid->out_buf_size;

	if (inst->disable_gentest)
		return 0;

	gentest_fill(inst->width, inst->height, to, vid->out_buf_size);

	return 0;
}

/* This threads is responsible for reading input file or stream and
 * feeding video enccoder with consecutive frames to encode */
static void *input_thread_func(void *args)
{
	struct instance *i = (struct instance *)args;
	struct video *vid = &i->video;
	unsigned int used, fs, n;
	int ret;

	while (!i->error && !i->finish) {
		n = 0;
		pthread_mutex_lock(&i->lock);
		while (n < vid->out_buf_cnt && vid->out_buf_flag[n])
			n++;
		pthread_mutex_unlock(&i->lock);

		if (n < vid->out_buf_cnt) {

			ret = input_read(i, n, &used, &fs);
			if (ret)
				err("cannot read from input file");

			if (vid->total_encoded >= i->num_frames_to_save) {
				i->finish = 1;
				fs = 0;
			}

			ret = video_setup_extra_control_out(i,n);
			if (ret)
				{
				err("Unable to video_setup_extra_control_out: %s\n",strerror(errno));
				return NULL;
				}
			ret = video_queue_buf_out(i, n, fs);
			if (ret)
				continue;

			ret = ioctl(vid->request_fd_out[n], MEDIA_REQUEST_IOC_QUEUE, NULL);
			if (ret < 0) {
				err("Unable to queue media request: %s\n",strerror(errno));
				return NULL;
			}
			pthread_mutex_lock(&i->lock);
			vid->out_buf_flag[n] = 1;
			pthread_mutex_unlock(&i->lock);

			dbg("queued output buffer %d", n);
		} else {
			pthread_mutex_lock(&i->lock);
			pthread_cond_wait(&i->cond, &i->lock);
			pthread_mutex_unlock(&i->lock);
		}
	}

	return NULL;
}

static void *main_thread_func(void *args)
{
	struct instance *i = (struct instance *)args;
	struct video *vid = &i->video;
	struct pollfd pfd;
	short revents;
	unsigned int n, finished;
	int ret;

	pfd.fd = vid->fd;
	pfd.events = POLLIN | POLLRDNORM | POLLOUT | POLLWRNORM |
		     POLLRDBAND | POLLPRI;

	fprintf(stdout, "encoded frame ");

	while (1) {
		ret = poll(&pfd, 1, 10000);
		if (!ret) {
			err("poll timeout");
			break;
		} else if (ret < 0) {
			err("poll error");
			break;
		}

		revents = pfd.revents;

		if (revents & POLLPRI)
			handle_v4l_events(vid);

		if (revents & (POLLIN | POLLRDNORM)) {
			unsigned int bytesused, dataoffset;

			/* capture buffer is ready */

			dbg("dequeuing capture buffer");

			ret = video_dequeue_capture(i, &n, &finished,
						    &bytesused, &dataoffset,
						    NULL);
			if (ret < 0)
				goto next_event;

			pthread_mutex_lock(&i->lock);
			vid->cap_buf_flag[n] = 0;
			pthread_mutex_unlock(&i->lock);

			fprintf(stdout, "%03ld\b\b\b", vid->total_encoded);
			fflush(stdout);

			if (finished)
				break;

			vid->total_encoded++;

			uint8_t *data = (uint8_t*)vid->cap_buf_addr[n][0];
			data += dataoffset;

			save_encoded(i, (const void *)data, bytesused,
				     i->codec == V4L2_PIX_FMT_VP8);

			ret = video_queue_buf_cap(i, n);
			if (!ret) {
				pthread_mutex_lock(&i->lock);
				vid->cap_buf_flag[n] = 1;
				pthread_mutex_unlock(&i->lock);
			}
		}

next_event:
		if (revents & (POLLOUT | POLLWRNORM)) {

			if (i->finish)
				continue;

			dbg("dequeuing output buffer");

			ret = video_dequeue_output(i, &n);
			if (ret < 0) {
				err("dequeue output buffer fail");
			} else {
				pthread_mutex_lock(&i->lock);
				vid->out_buf_flag[n] = 0;
				pthread_mutex_unlock(&i->lock);
				pthread_cond_signal(&i->cond);
			}

			dbg("dequeued output buffer %d", n);
		}
	}

	return NULL;
}

int main(int argc, char **argv)
{
	struct instance inst;
	struct video *vid = &inst.video;
	pthread_t input_thread, main_thread;
	int ret, n;

	ret = parse_args(&inst, argc, argv);
	if (ret) {
		print_usage(argv[0]);
		return 1;
	}

	info("encoding resolution is %dx%d", inst.width, inst.height);

	pthread_mutex_init(&inst.lock, 0);

	pthread_condattr_init(&inst.attr);
	pthread_cond_init(&inst.cond, &inst.attr);

	vid->total_encoded = 0;

	ret = video_open(&inst, inst.video.name);
	if (ret)
		goto err;

	ret = subscribe_for_events(vid->fd);
	if (ret)
		goto err;

	ret = video_setup_capture(&inst, 2, inst.width, inst.height);
	if (ret)
		goto err;

	ret = video_setup_output(&inst, inst.codec, STREAM_BUFFER_SIZE, 1);
	if (ret)
		goto err;

	info("gentest init: %ux%u, bpl:%u, crop_w:%u, crop_h:%u",
		vid->out_w, vid->out_h, vid->out_bytesperline,
		inst.width, inst.height);

	ret = gentest_init(vid->out_w, vid->out_h, vid->out_bytesperline,
			   inst.width, inst.height,
			   vid->out_buf_size);
	if (ret)
		goto err;

	ret = video_set_control(&inst);
	if (ret)
		goto err;

	for (n = 0; n < vid->cap_buf_cnt; n++) {
		ret = video_queue_buf_cap(&inst, n);
		if (ret)
			goto err;

		vid->cap_buf_flag[n] = 1;
	}

	ret = video_stream(&inst, V4L2_BUF_TYPE_VIDEO_CAPTURE_MPLANE,
			   VIDIOC_STREAMON);
	if (ret)
		goto err;

	ret = video_stream(&inst, V4L2_BUF_TYPE_VIDEO_OUTPUT_MPLANE,
			   VIDIOC_STREAMON);
	if (ret)
		goto err;

	dbg("Launching threads");

	if (pthread_create(&input_thread, NULL, input_thread_func, &inst))
		goto err;

	if (pthread_create(&main_thread, NULL, main_thread_func, &inst))
		goto err;

	pthread_join(input_thread, 0);
	pthread_join(main_thread, 0);

	dbg("Threads have finished");

	video_stop(&inst);

	info("Total frames encoded %ld", vid->total_encoded);

	pthread_mutex_destroy(&inst.lock);
	cleanup(&inst);
	gentest_deinit();

	return 0;
err:
	pthread_mutex_destroy(&inst.lock);
	pthread_cond_destroy(&inst.cond);
	pthread_condattr_destroy(&inst.attr);
	cleanup(&inst);
	gentest_deinit();
	return 1;
}
