/*
 * V4L2 Codec decoding example application
 * Kamil Debski <k.debski@samsung.com>
 *
 *
 * Copyright 2012 Samsung Electronics Co., Ltd.
 * Copyright (c) 2015 Linaro Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include <linux/videodev2.h>
#include <linux/media.h>
#include <linux/v4l2-controls.h>
#include <fcntl.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/time.h>
#include <unistd.h>
#include <errno.h>

#include "common.h"

#define V4L2_BUF_FLAG_LAST			0x00100000

static char *dbg_type[2] = {"OUTPUT", "CAPTURE"};
static char *dbg_status[2] = {"ON", "OFF"};
#define ARRAY_SIZE(arr) (sizeof(arr) / sizeof((arr)[0]))

/* check is it a decoder video device */
static int is_video_encoder(int fd, const char *name)
{
	struct v4l2_capability cap;
	struct v4l2_fmtdesc fdesc;
	int found = 0;

	memzero(cap);
	if (ioctl(fd, VIDIOC_QUERYCAP, &cap) < 0) {
		err("Failed to verify capabilities: %m");
		return -1;
	}

	dbg("caps (%s): driver=\"%s\" bus_info=\"%s\" card=\"%s\" "
	    "version=%u.%u.%u", name, cap.driver, cap.bus_info, cap.card,
	    (cap.version >> 16) & 0xff,
	    (cap.version >> 8) & 0xff,
	     cap.version & 0xff);

	if (!(cap.capabilities & V4L2_CAP_STREAMING) ||
	    !(cap.capabilities & V4L2_CAP_VIDEO_M2M_MPLANE)) {
		err("Insufficient capabilities for video device (is %s correct?)",
		    name);
		return -1;
	}

	memzero(fdesc);
	fdesc.type = V4L2_BUF_TYPE_VIDEO_OUTPUT_MPLANE;

	while (!ioctl(fd, VIDIOC_ENUM_FMT, &fdesc)) {
		dbg("  %s", fdesc.description);

		switch (fdesc.pixelformat) {
		case V4L2_PIX_FMT_NV12:
		case V4L2_PIX_FMT_NV21:
			found = 1;
			break;
		default:
			dbg("%s is not an encoder video device", name);
			return -1;
		}

		if (found)
			break;

		fdesc.index++;
	}

	found = 0;
	memzero(fdesc);
	fdesc.type = V4L2_BUF_TYPE_VIDEO_CAPTURE_MPLANE;

	while (!ioctl(fd, VIDIOC_ENUM_FMT, &fdesc)) {
		dbg("  %s", fdesc.description);

		switch (fdesc.pixelformat) {
		case V4L2_PIX_FMT_MPEG:
		case V4L2_PIX_FMT_H264:
		case V4L2_PIX_FMT_H263:
		case V4L2_PIX_FMT_MPEG1:
		case V4L2_PIX_FMT_MPEG2:
		case V4L2_PIX_FMT_MPEG4:
		case V4L2_PIX_FMT_XVID:
		case V4L2_PIX_FMT_VC1_ANNEX_G:
		case V4L2_PIX_FMT_VC1_ANNEX_L:
		case V4L2_PIX_FMT_VP8:
			found = 1;
			break;
		default:
			err("%s is not an encoder video device", name);
			return -1;
		}

		if (found)
			break;

		fdesc.index++;
	}

	return 0;
}

int video_open(struct instance *i, const char *name)
{
	char video_name[64];
	char media_name[64];
	unsigned idxM = 0, idxV = 0, v = 0;
	int ret, fd = -1, media_fd = -1;

		ret = sprintf(media_name, "/dev/media%d", idxM);
		if (ret < 0)
			return ret;

		err("open media device: %s", media_name);

		media_fd = open(media_name, O_RDWR, 0);
		if (media_fd < 0) {
			err("Failed to open media device: %s", media_name);
		return -1;
		}

		ret = sprintf(video_name, "/dev/video%d", idxV);
		if (ret < 0)
			return ret;

		err("open video device: %s", video_name);

		fd = open(video_name, O_RDWR, 0);
		if (fd < 0) {
			err("Failed to open video device: %s", video_name);
			return -1;
		}

		ret = is_video_encoder(fd, video_name);
		if (ret < 0) {
			close(fd);
			close(media_fd);
			return -1;
		}

	if (!ret)
		info("found encoder video device %s", video_name);

	if (ret < 0)
		fd = open(name, O_RDWR, 0);

	if (fd < 0) {
		err("Failed to open video device: %s", name);
		return -1;
	}

	i->video.fd = fd;
	i->video.media_fd = media_fd;

    return 0;
}

void video_close(struct instance *i)
{
	close(i->video.fd);
	close(i->video.media_fd);
}

int video_set_control(struct instance *i)
{
	struct v4l2_streamparm parm;
	struct v4l2_control cntrl;
	int ret;

	parm.type = V4L2_BUF_TYPE_VIDEO_OUTPUT_MPLANE;
	parm.parm.output.timeperframe.numerator = 1;
	if (i->framerate <= 120 && i->framerate >= 1)
		parm.parm.output.timeperframe.denominator = i->framerate;
	else
		parm.parm.output.timeperframe.denominator = 30;

	info("setting framerate: %u (%u/%u)", i->framerate,
		parm.parm.output.timeperframe.numerator,
		parm.parm.output.timeperframe.denominator);

	ret = ioctl(i->video.fd, VIDIOC_S_PARM, &parm);
	if (ret)
		err("set framerate (%s)", strerror(errno));

	cntrl.id = V4L2_CID_MPEG_VIDEO_BITRATE;
	if (i->bitrate <= 160000000 && i->bitrate >= 32000)
		cntrl.value = i->bitrate;
	else
		cntrl.value = 10 * 1024 * 1024;

	info("setting bitrate: %u", cntrl.value);

	ret = ioctl(i->video.fd, VIDIOC_S_CTRL, &cntrl);
	if (ret)
		err("set control - bitrate (%s)", strerror(errno));

	if (i->codec == V4L2_PIX_FMT_H264) {
		memset(&cntrl, 0, sizeof(cntrl));
		cntrl.id = V4L2_CID_MPEG_VIDEO_H264_PROFILE;
		cntrl.value = V4L2_MPEG_VIDEO_H264_PROFILE_HIGH;
		info("setting h264 profile: %u", cntrl.value);
		ret = ioctl(i->video.fd, VIDIOC_S_CTRL, &cntrl);
		if (ret)
			err("set control - profile (%s)", strerror(errno));

		memset(&cntrl, 0, sizeof(cntrl));
		cntrl.id = V4L2_CID_MPEG_VIDEO_H264_LEVEL;
		cntrl.value = V4L2_MPEG_VIDEO_H264_LEVEL_5_0;
		info("setting h264 level: %u", cntrl.value);
		ret = ioctl(i->video.fd, VIDIOC_S_CTRL, &cntrl);
		if (ret)
			err("set control - level (%s)", strerror(errno));

		if (i->num_bframes && i->num_bframes < 4) {
			memset(&cntrl, 0, sizeof(cntrl));
			cntrl.id = V4L2_CID_MPEG_VIDEO_B_FRAMES;
			cntrl.value = i->num_bframes;
			info("setting num b frames: %u", cntrl.value);
			ret = ioctl(i->video.fd, VIDIOC_S_CTRL, &cntrl);
			if (ret)
				err("set control - num b frames (%s)",
					strerror(errno));
		}
	}

	if (i->codec == V4L2_PIX_FMT_HEVC) {
		if (i->num_bframes && i->num_bframes < 4) {
			memset(&cntrl, 0, sizeof(cntrl));
			cntrl.id = V4L2_CID_MPEG_VIDEO_B_FRAMES;
			cntrl.value = i->num_bframes;
			info("setting num b frames: %u", cntrl.value);
			ret = ioctl(i->video.fd, VIDIOC_S_CTRL, &cntrl);
			if (ret)
				err("set control - num b frames (%s)",
					strerror(errno));
		}
	}

	if (i->codec == V4L2_PIX_FMT_VP8) {
		memset(&cntrl, 0, sizeof(cntrl));
		cntrl.id = V4L2_CID_MPEG_VIDEO_VPX_PROFILE;
		cntrl.value = 2;
		info("setting VP8 profile: %u", cntrl.value);
		ret = ioctl(i->video.fd, VIDIOC_S_CTRL, &cntrl);
		if (ret)
			err("set control - profile (%s)", strerror(errno));
	}

#if 0
	struct v4l2_ext_controls ctrls;
	struct v4l2_ext_control ctrl[32];
	int c = 0;

	memset (&ctrls, 0, sizeof(ctrls));
	ctrls.controls = ctrl;
	ctrls.ctrl_class = V4L2_CTRL_CLASS_MPEG;

	ctrl[c].id = V4L2_CID_MPEG_VIDEO_H264_PROFILE;
	ctrl[c].value = V4L2_MPEG_VIDEO_H264_PROFILE_HIGH;
	c++;

	ctrl[c].id = V4L2_CID_MPEG_VIDEO_H264_LEVEL;
	ctrl[c].value = V4L2_MPEG_VIDEO_H264_LEVEL_5_0;
	c++;

	ctrl[c].id = V4L2_CID_MPEG_VIDEO_BITRATE_MODE;
	ctrl[c].value = V4L2_MPEG_VIDEO_BITRATE_MODE_VBR;
	c++;

	ctrl[c].id = V4L2_CID_MPEG_VIDEO_GOP_SIZE;
	ctrl[c].value = 1;
	c++;

	/*
	 * intra_period = pframes + bframes + 1
	 * bframes/pframes must be integer
	 */

	ctrl[c].id = V4L2_CID_MPEG_VIDEO_B_FRAMES;
	ctrl[c].value = 0;
	c++;

//	ctrl[c].id = V4L2_CID_MPEG_VIDC_VIDEO_NUM_P_FRAMES;
//	ctrl[c].value = 2 * 15 - 1;
//	c++;

	ctrls.count = c;

	ret = ioctl(i->video.fd, VIDIOC_S_EXT_CTRLS, &ctrls);
	if (ret)
		err("set ext controls (%s)", strerror(errno));
#endif
	return 0;
}

int set_extra_control(int video_fd, int request_fd, unsigned int id,
		       void *data, unsigned int size)
{
	struct v4l2_ext_control control;
	struct v4l2_ext_controls controls;
	int rc;
	err("set_extra_control");
	memset(&control, 0, sizeof(control));
	memset(&controls, 0, sizeof(controls));

	control.id = id;
	control.ptr = data;
	control.size = size;

	controls.controls = &control;
	controls.count = 1;

	if (request_fd >= 0) {
		controls.which = V4L2_CTRL_WHICH_REQUEST_VAL;
		controls.request_fd = request_fd;
	}

	rc = ioctl(video_fd, VIDIOC_S_EXT_CTRLS, &controls);
	if (rc < 0) {
		err("Unable to set control: %d %s\n", errno,strerror(errno));
		return -1;
	}

	return 0;
}

int video_setup_extra_control_out(struct instance *inst, unsigned int index)
{
	struct video *vid = &inst->video;
	int rc;
	unsigned int i;
	err("video_setup_extra_control_out index %d",index);
	struct v4l2_ctrl_venus_metadata {
	unsigned int index;
	//__u8	hdr10plus[16];
	} hdr10plusInfo[] = {{1},{2},{3},{4},{5},{6},{7},{8},{9}};

	if (index >= vid->out_buf_cnt) {
		err("Tried to queue a non exisiting buffer");
		return -1;
	}
	struct {
		char *description;
		unsigned int id;
		void *data;
		unsigned int size;
	} controls[] = {{"HDR10", V4L2_CID_MPEG_VIDEO_VENUS_METADATA , &hdr10plusInfo[index+vid->cap_buf_cnt], sizeof(hdr10plusInfo)}};
	for (i = 0; i < ARRAY_SIZE(controls); i++) {
		struct v4l2_ctrl_venus_metadata *meta = controls[i].data;
		err("set_extra_control : vid->fd %d vid->request_fd_out[%d] %d meta index %d",vid->fd,index,vid->request_fd_out[index],meta->index);
		rc = set_extra_control(vid->fd, vid->request_fd_out[index], controls[i].id, controls[i].data,
				 controls[i].size);
		if (rc < 0) {
			err("Unable to set %s control\n", controls[i].description);
			fprintf(stderr, "Unable to set %s control\n",strerror(errno));
			return -1;
		}
	}
	return rc;
}

int video_setup_extra_control_cap(struct instance *inst, unsigned int index)
{
	struct video *vid = &inst->video;
	int rc;
	unsigned int i;
	err("video_setup_extra_control_cap index %d",index);
	struct v4l2_ctrl_venus_metadata {
	unsigned int index;
	//__u8	hdr10plus[16];
	} hdr10plusInfo[] = {{1},{2},{3},{4},{5},{6},{7},{8},{9}};
		
	if (index >= vid->cap_buf_cnt) {
		err("Tried to queue a non exisiting buffer");
		return -1;
	}
	struct {
		char *description;
		unsigned int id;
		void *data;
		unsigned int size;
	} controls[] = {{"HDR10", V4L2_CID_MPEG_VIDEO_VENUS_METADATA , &hdr10plusInfo[index], sizeof(hdr10plusInfo)}};
	for (i = 0; i < ARRAY_SIZE(controls); i++) {
		struct v4l2_ctrl_venus_metadata *meta = controls[i].data;
		err("set_extra_control : vid->fd %d vid->request_fd_cap[%d] %d meta index %d",vid->fd,index,vid->request_fd_cap[index],meta->index);	
		rc = set_extra_control(vid->fd, vid->request_fd_cap[index], controls[i].id, controls[i].data,
				 controls[i].size);
		if (rc < 0) {
			err("Unable to set %s control\n", controls[i].description);
			fprintf(stderr, "Unable to set %s control\n",strerror(errno));
			return -1;
		}
	}
	return rc;
}

int video_export_buf(struct instance *i, unsigned int index)
{
	struct video *vid = &i->video;
	struct v4l2_exportbuffer expbuf;
	int num_planes = CAP_PLANES;
	int n;

	for (n = 0; n < num_planes; n++) {
		memset(&expbuf, 0, sizeof(expbuf));

		expbuf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE_MPLANE;
		expbuf.index = index;
		expbuf.flags = O_CLOEXEC | O_RDWR;
		expbuf.plane = n;

		if (ioctl(vid->fd, VIDIOC_EXPBUF, &expbuf) < 0) {
			err("Failed to export CAPTURE buffer index%d (%s)",
			    index, strerror(errno));
			return -1;
		}

		info("Exported CAPTURE buffer index%d (plane%d) with fd %d",
		     index, n, expbuf.fd);
	}

	return 0;
}

static int video_queue_buf(struct instance *i, unsigned int index,
			   unsigned int l1, unsigned int l2,
			   unsigned int type, unsigned int nplanes)
{
	struct video *vid = &i->video;
	struct v4l2_buffer buf;
	struct v4l2_plane planes[2];
	struct timeval tv;
	int ret;
	int request_fd = -1;

	memzero(buf);
	memset(planes, 0, sizeof(planes));
	buf.type = type;
	buf.memory = V4L2_MEMORY_MMAP;
	buf.index = index;
	buf.length = nplanes;
	buf.m.planes = planes;

	buf.m.planes[0].bytesused = l1;
	buf.m.planes[1].bytesused = l2;

	buf.m.planes[0].data_offset = 0;
	buf.m.planes[1].data_offset = 0;

	if(type == V4L2_BUF_TYPE_VIDEO_OUTPUT_MPLANE)
		request_fd = vid->request_fd_out[index];
	else
		request_fd = vid->request_fd_cap[index];
	if (request_fd >= 0) {
		err("request_fd %d", request_fd);
		buf.flags = V4L2_BUF_FLAG_REQUEST_FD;
		buf.request_fd = request_fd;
	}

	if (type == V4L2_BUF_TYPE_VIDEO_CAPTURE_MPLANE) {
		buf.m.planes[0].length = vid->cap_buf_size[0];
	} else {
		buf.m.planes[0].length = vid->out_buf_size;
		if (l1 == 0) {
			buf.m.planes[0].bytesused = 1;
			buf.flags |= V4L2_BUF_FLAG_LAST;
		}

		ret = gettimeofday(&tv, NULL);
		if (ret)
			err("getting timeofday (%s)", strerror(errno));

		buf.timestamp = tv;
	}

	ret = ioctl(vid->fd, VIDIOC_QBUF, &buf);
	if (ret) {
		err("Failed to queue buffer (index=%d) on %s %u (%s)",
		    buf.index,
		    dbg_type[type==V4L2_BUF_TYPE_VIDEO_CAPTURE_MPLANE],
		    errno,
		    strerror(errno));
		return -1;
	}

	dbg("  Queued buffer on %s queue with index %d",
	    dbg_type[type==V4L2_BUF_TYPE_VIDEO_CAPTURE_MPLANE], buf.index);

	return 0;
}

int video_queue_buf_out(struct instance *i, unsigned int n, unsigned int length)
{
	struct video *vid = &i->video;

	if (n >= vid->out_buf_cnt) {
		err("Tried to queue a non exisiting buffer");
		return -1;
	}

	return video_queue_buf(i, n, length, 0,
			       V4L2_BUF_TYPE_VIDEO_OUTPUT_MPLANE,
			       OUT_PLANES);
}

int video_queue_buf_cap(struct instance *i, unsigned int index)
{
	int ret;
	struct video *vid = &i->video;

	if (index >= vid->cap_buf_cnt) {
		err("Tried to queue a non exisiting buffer");
		return -1;
	}

	ret = video_setup_extra_control_cap(i,index);
	if (ret)
	{
		err("Unable to video_setup_extra_control_out: %s\n",strerror(errno));
		return -1;
	}
	err("queuing output buffer %d", index);
	ret = video_queue_buf(i, index, vid->cap_buf_size[0],
			       vid->cap_buf_size[1],
			       V4L2_BUF_TYPE_VIDEO_CAPTURE_MPLANE,
			       CAP_PLANES);
	if (ret)
	{
		err("Unable to video_queue_buf out : %s\n",strerror(errno));
		return -1;
	}
	ret = ioctl(vid->request_fd_cap[index], MEDIA_REQUEST_IOC_QUEUE, NULL);
	if (ret < 0) {
		err("Unable to queue media request: %s\n",strerror(errno));
		return -1;
	}
	return 0;
}

static int video_dequeue_buf(struct instance *i, struct v4l2_buffer *buf)
{
	struct video *vid = &i->video;
	int ret;

	ret = ioctl(vid->fd, VIDIOC_DQBUF, buf);
	if (ret < 0) {
		err("Failed to dequeue buffer (%s)", strerror(errno));
		return -errno;
	}

	dbg("Dequeued buffer on %s queue with index %d (flags:%x, bytesused:%d)",
	    dbg_type[buf->type == V4L2_BUF_TYPE_VIDEO_CAPTURE_MPLANE],
	    buf->index, buf->flags, buf->m.planes[0].bytesused);

	return 0;
}

int video_dequeue_output(struct instance *i, unsigned int *n)
{
	struct v4l2_buffer buf;
	struct v4l2_plane planes[OUT_PLANES];
	int ret;
	int request_fd = -1;

	memzero(buf);
	buf.type = V4L2_BUF_TYPE_VIDEO_OUTPUT_MPLANE;
	buf.memory = V4L2_MEMORY_MMAP;
	buf.m.planes = planes;
	buf.length = OUT_PLANES;

	ret = video_dequeue_buf(i, &buf);
	if (ret < 0)
		return ret;

	*n = buf.index;
	request_fd = i->video.request_fd_out[*n];
	ret = ioctl(request_fd, MEDIA_REQUEST_IOC_REINIT, NULL);
	if (ret < 0) {
		fprintf(stderr, "Unable to reinit media request for fd %d: %s\n",
			request_fd,strerror(errno));
		//return -1;
	}
	return 0;
}

int video_dequeue_capture(struct instance *i, unsigned int *n,
			  unsigned int *finished, unsigned int *bytesused,
			  unsigned int *data_offset, unsigned int *buf_flags)
{
	struct v4l2_buffer buf;
	struct v4l2_plane planes[CAP_PLANES];
	int request_fd = -1;
	int ret;

	memzero(buf);
	buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE_MPLANE;
	buf.memory = V4L2_MEMORY_MMAP;
	buf.m.planes = planes;
	buf.length = CAP_PLANES;

	if (video_dequeue_buf(i, &buf))
		return -1;

	*finished = 0;

	if (buf.flags & V4L2_BUF_FLAG_LAST || buf.m.planes[0].bytesused == 0)
		*finished = 1;

	*bytesused = buf.m.planes[0].bytesused;
	*n = buf.index;
	*data_offset = buf.m.planes[0].data_offset;

	if (buf_flags)
		*buf_flags = buf.flags;

	request_fd = i->video.request_fd_cap[*n];
	ret = ioctl(request_fd, MEDIA_REQUEST_IOC_REINIT, NULL);
	if (ret < 0) {
		fprintf(stderr, "Unable to reinit media request for fd %d: %s\n",
			request_fd,strerror(errno));
		//return -1;
	}

	return 0;
}

int video_stream(struct instance *i, enum v4l2_buf_type type,
		 unsigned int status)
{
	struct video *vid = &i->video;
	int ret;

	ret = ioctl(vid->fd, status, &type);
	if (ret) {
		err("Failed to change streaming (type=%s, status=%s)",
		    dbg_type[type == V4L2_BUF_TYPE_VIDEO_CAPTURE_MPLANE],
		    dbg_status[status == VIDIOC_STREAMOFF]);
		return -1;
	}

	dbg("Stream %s on %s queue", dbg_status[status==VIDIOC_STREAMOFF],
	    dbg_type[type == V4L2_BUF_TYPE_VIDEO_CAPTURE_MPLANE]);

	return 0;
}

int video_stop(struct instance *i)
{
	int ret;

	ret = video_stream(i, V4L2_BUF_TYPE_VIDEO_CAPTURE_MPLANE,
			   VIDIOC_STREAMOFF);
	if (ret < 0)
		err("STREAMOFF CAPTURE queue failed (%s)", strerror(errno));

	ret = video_stream(i, V4L2_BUF_TYPE_VIDEO_OUTPUT_MPLANE,
			   VIDIOC_STREAMOFF);
	if (ret < 0)
		err("STREAMOFF OUTPUT queue failed (%s)", strerror(errno));

	return 0;
}

int video_setup_capture(struct instance *i, unsigned int count, unsigned int w,
			unsigned int h)
{
	struct video *vid = &i->video;
	struct v4l2_format fmt;
	struct v4l2_requestbuffers reqbuf;
	struct v4l2_buffer buf;
	struct v4l2_plane planes[CAP_PLANES];
	unsigned int n;
	int ret;
	int request_fd = -1;

	memzero(fmt);
	fmt.type = V4L2_BUF_TYPE_VIDEO_CAPTURE_MPLANE;
	fmt.fmt.pix_mp.height = h;
	fmt.fmt.pix_mp.width = w;
	fmt.fmt.pix_mp.pixelformat = i->codec;
	ret = ioctl(vid->fd, VIDIOC_S_FMT, &fmt);
	if (ret) {
		err("CAPTURE: S_FMT failed (%ux%u) (%s)", w, h, strerror(errno));
		return -1;
	}

	info("CAPTURE: Set format %ux%u, sizeimage %u, bpl %u, pixelformat:%x",
	    fmt.fmt.pix_mp.width, fmt.fmt.pix_mp.height,
	    fmt.fmt.pix_mp.plane_fmt[0].sizeimage,
	    fmt.fmt.pix_mp.plane_fmt[0].bytesperline,
	    fmt.fmt.pix_mp.pixelformat);

	vid->cap_w = fmt.fmt.pix_mp.width;
	vid->cap_h = fmt.fmt.pix_mp.height;

	vid->cap_buf_size[0] = fmt.fmt.pix_mp.plane_fmt[0].sizeimage;
	vid->cap_buf_size[1] = fmt.fmt.pix_mp.plane_fmt[1].sizeimage;

	vid->cap_buf_cnt_min = 1;
	vid->cap_buf_queued = 0;

	memzero(reqbuf);
	reqbuf.count = count;
	reqbuf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE_MPLANE;
	reqbuf.memory = V4L2_MEMORY_MMAP;

	ret = ioctl(vid->fd, VIDIOC_REQBUFS, &reqbuf);
	if (ret) {
		err("CAPTURE: REQBUFS failed (%s)", strerror(errno));
		return -1;
	}

	vid->cap_buf_cnt = reqbuf.count;

	info("CAPTURE: Number of buffers %u (requested %u)",
		vid->cap_buf_cnt, count);

	for (n = 0; n < vid->cap_buf_cnt; n++) {
		memzero(buf);
		memset(planes, 0, sizeof(planes));
		buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE_MPLANE;
		buf.memory = V4L2_MEMORY_MMAP;
		buf.index = n;
		buf.m.planes = planes;
		buf.length = CAP_PLANES;

		ret = ioctl(vid->fd, VIDIOC_QUERYBUF, &buf);
		if (ret) {
			err("CAPTURE: QUERYBUF failed (%s)", strerror(errno));
			return -1;
		}

		vid->cap_buf_off[n][0] = buf.m.planes[0].m.mem_offset;

		vid->cap_buf_addr[n][0] = mmap(NULL, buf.m.planes[0].length,
					       PROT_READ | PROT_WRITE,
					       MAP_SHARED,
					       vid->fd,
					       buf.m.planes[0].m.mem_offset);

		if (vid->cap_buf_addr[n][0] == MAP_FAILED) {
			err("CAPTURE: MMAP failed (%s)", strerror(errno));
			return -1;
		}

		vid->cap_buf_size[0] = buf.m.planes[0].length;

		ret = ioctl(vid->media_fd, MEDIA_IOC_REQUEST_ALLOC, &request_fd);
		if (ret) {
			err("Unable to allocate media request %u (%s)", errno,strerror(errno));
			return -1;
		}
		vid->request_fd_cap[n] = request_fd;
		err("request_fd %d vid->request_fd_out[%d] %d", request_fd,n,vid->request_fd_cap[n]);
	}

	info("CAPTURE: Succesfully mmapped %u buffers", n);

	return 0;
}

int video_setup_output(struct instance *i, unsigned long codec,
		       unsigned int size, unsigned int count)
{
	struct video *vid = &i->video;
	struct v4l2_format fmt, try_fmt, g_fmt;
	struct v4l2_requestbuffers reqbuf;
	struct v4l2_buffer buf;
	struct v4l2_plane planes[OUT_PLANES];
	int ret;
	int n;
	int request_fd = -1;

	memzero(try_fmt);
	try_fmt.type = V4L2_BUF_TYPE_VIDEO_OUTPUT_MPLANE;
	try_fmt.fmt.pix_mp.width = i->width;
	try_fmt.fmt.pix_mp.height = i->height;
	try_fmt.fmt.pix_mp.pixelformat = V4L2_PIX_FMT_NV12;

	ret = ioctl(vid->fd, VIDIOC_TRY_FMT, &try_fmt);
	if (ret) {
		err("OUTPUT: TRY_FMT failed (%ux%u) (%s)",
			try_fmt.fmt.pix_mp.width,
			try_fmt.fmt.pix_mp.height, strerror(errno));
		return -1;
	}

	info("OUTPUT: Try format %ux%u, sizeimage %u, bpl %u",
	     try_fmt.fmt.pix_mp.width, try_fmt.fmt.pix_mp.height,
	     try_fmt.fmt.pix_mp.plane_fmt[0].sizeimage,
	     try_fmt.fmt.pix_mp.plane_fmt[0].bytesperline);

	memzero(fmt);
	fmt.type = V4L2_BUF_TYPE_VIDEO_OUTPUT_MPLANE;
	fmt.fmt.pix_mp.width = i->width;
	fmt.fmt.pix_mp.height = i->height;
	fmt.fmt.pix_mp.pixelformat = V4L2_PIX_FMT_NV12;

	ret = ioctl(vid->fd, VIDIOC_S_FMT, &fmt);
	if (ret) {
		err("OUTPUT: S_FMT failed %ux%u (%s)", fmt.fmt.pix_mp.width,
			fmt.fmt.pix_mp.height, strerror(errno));
		return -1;
	}

	info("OUTPUT: Set format %ux%u, sizeimage %u, (requested=%u), bpl %u",
	     fmt.fmt.pix_mp.width, fmt.fmt.pix_mp.height,
	     fmt.fmt.pix_mp.plane_fmt[0].sizeimage, size,
	     fmt.fmt.pix_mp.plane_fmt[0].bytesperline);

	vid->out_buf_size = fmt.fmt.pix_mp.plane_fmt[0].sizeimage;

	memzero(g_fmt);
	g_fmt.type = V4L2_BUF_TYPE_VIDEO_OUTPUT_MPLANE;
	g_fmt.fmt.pix_mp.pixelformat = V4L2_PIX_FMT_NV12;

	ret = ioctl(vid->fd, VIDIOC_G_FMT, &g_fmt);
	if (ret) {
		err("OUTPUT: G_FMT failed (%s)", strerror(errno));
		return -1;
	}

	info("OUTPUT: Get format %ux%u, sizeimage %u, bpl %u",
	     g_fmt.fmt.pix_mp.width, g_fmt.fmt.pix_mp.height,
	     g_fmt.fmt.pix_mp.plane_fmt[0].sizeimage,
	     g_fmt.fmt.pix_mp.plane_fmt[0].bytesperline);

	vid->out_w = g_fmt.fmt.pix_mp.width;
	vid->out_h = g_fmt.fmt.pix_mp.height;
	vid->out_bytesperline = g_fmt.fmt.pix_mp.plane_fmt[0].bytesperline;

	memzero(reqbuf);
	reqbuf.count = count;
	reqbuf.type = V4L2_BUF_TYPE_VIDEO_OUTPUT_MPLANE;
	reqbuf.memory = V4L2_MEMORY_MMAP;

	ret = ioctl(vid->fd, VIDIOC_REQBUFS, &reqbuf);
	if (ret) {
		err("OUTPUT: REQBUFS failed (%s)", strerror(errno));
		return -1;
	}

	vid->out_buf_cnt = reqbuf.count;

	info("OUTPUT: Number of buffers %u (requested %u)",
	     vid->out_buf_cnt, count);

	for (n = 0; n < vid->out_buf_cnt; n++) {
		memzero(buf);
		memset(planes, 0, sizeof(planes));
		buf.type = V4L2_BUF_TYPE_VIDEO_OUTPUT_MPLANE;
		buf.memory = V4L2_MEMORY_MMAP;
		buf.index = n;
		buf.m.planes = planes;
		buf.length = OUT_PLANES;

		ret = ioctl(vid->fd, VIDIOC_QUERYBUF, &buf);
		if (ret) {
			err("OUTPUT: QUERYBUF failed (%s)", strerror(errno));
			return -1;
		}

		vid->out_buf_off[n] = buf.m.planes[0].m.mem_offset;
		vid->out_buf_size = buf.m.planes[0].length;

		vid->out_buf_addr[n] = mmap(NULL, buf.m.planes[0].length,
					    PROT_READ | PROT_WRITE, MAP_SHARED,
					    vid->fd,
					    buf.m.planes[0].m.mem_offset);

		if (vid->out_buf_addr[n] == MAP_FAILED) {
			err("OUTPUT: MMAP failed (%s)", strerror(errno));
			return -1;
		}

		vid->out_buf_flag[n] = 0;

		ret = ioctl(vid->media_fd, MEDIA_IOC_REQUEST_ALLOC, &request_fd);
		if (ret) {
			err("Unable to allocate media request %u (%s)", errno,strerror(errno));
			return -1;
		}
		vid->request_fd_out[n] = request_fd;
		err("request_fd %d vid->request_fd_out[%d] %d", request_fd,n,vid->request_fd_out[n]);
	}

	info("OUTPUT: Succesfully mmapped %u buffers", n);

	return 0;
}
